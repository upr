# -*- encoding: binary -*-

ENV["VERSION"] or abort "VERSION= must be specified"
manifest = File.readlines('.manifest').map! { |x| x.chomp! }

Gem::Specification.new do |s|
  s.name = %q{upr}
  s.version = ENV["VERSION"]

  s.authors = ["upr hackers"]
  s.date = Time.now.utc.strftime('%Y-%m-%d')
  s.description = File.read("README").split(/\n\n/)[1]
  s.email = %q{upr@librelist.com}
  s.executables = %w(upr-drb)

  s.extra_rdoc_files = File.readlines('.document').map! do |x|
    x.chomp!
    if File.directory?(x)
      manifest.grep(%r{\A#{x}/})
    elsif File.file?(x)
      x
    else
      nil
    end
  end.flatten.compact

  s.files = manifest
  s.homepage = %q{http://upr.bogomips.org/}
  s.summary = %q{Upload Progress for Rack}
  s.rdoc_options = [ "-Na", "-t", "upr - #{s.summary}" ]
  s.require_paths = %w(lib)
  s.rubyforge_project = %q{rainbows}

  s.add_dependency(%q<moneta>, '~>0.7')
  s.add_dependency(%q<rack>)

  # Folks on intranets sharing humongous files can use Unicorn, too
  # s.add_dependency(%q<rainbows>)

  # s.licenses = %w(GPLv2 Ruby) # accessor not compatible with older Rubygems
end
