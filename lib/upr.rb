# -*- encoding: binary -*-

require 'moneta'
require 'upr/status'
module Upr

  # Upr version number
  VERSION = '0.3.0'

  autoload :Monitor, 'upr/monitor'
  autoload :Params, 'upr/params'
  autoload :InputWrapper, 'upr/input_wrapper'
  autoload :JSON, 'upr/json'

  # Initializes a new instance of Upr::InputWrapper.  Usage in config.ru:
  #
  #   use Upr, :path_info => %w(/upload),
  #            :drb => "druby://192.168.0.1:666",
  #            :frequency => 1
  #
  # This middleware MUST be be loaded before any parameter parsers
  # like ActionController::ParamsParser in Rails.
  #
  # For use in RAILS_ROOT/config/environment.rb, the following
  # works insdie the Rails::Initializer.run block:
  #
  #   config.middleware.insert_before('ActionController::ParamsParser',
  #       'Upr', :path_info => '/', :drb => "druby://192.168.0.1:666")
  #
  def self.new(*args)
    InputWrapper.new(*args)
  end

end
