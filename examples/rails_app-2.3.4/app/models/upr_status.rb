class UprStatus < ActiveRecord::Base

  class << self
    def read(upid)
      find_by_upid(upid)
    end

    def start(upid, length)
      # this must be a find_or_create_by since some users have twitchy
      # fingers and hit the upload button prematurely
      find_or_create_by_upid(upid) do |x|
        x.length = length
        x.time = Time.now.to_i
        x.seen = 0
      end
    end

    def incr(upid, nr)
      transaction do
        if rv = find_by_upid(upid)
          rv.time = Time.now.to_i
          rv.seen += nr if rv.seen >= 0
          rv.save
          rv
        end
      end
    end

    def error!(upid)
      transaction do
        if rv = find_by_upid(upid)
          rv.time = Time.now.to_i
          rv.seen = -1
          rv.save
          rv
        end
      end
    end

    def finish(upid)
      transaction do
        if rv = find_by_upid(upid)
          rv.time = Time.now.to_i
          rv.length ||= rv.seen
          rv.seen = rv.length
          rv.save
          rv
        end
      end
    end

    def gc
      cutoff = Time.now.to_i - Upr::Monitor::OPT[:expires_in]
      delete_all "time < #{cutoff}"
    end
  end

  include Upr::StatusMethods
end
