class ProgressController < ApplicationController
  def index
    opt = {
      :backend => $upr,
      :frequency => 0.5,
      :env => request.env,
    }
    response.headers.update(Upr::JSON::RESPONSE_HEADERS)
    render(Upr::JSON.new(opt).rails_render_options)
  end
end
