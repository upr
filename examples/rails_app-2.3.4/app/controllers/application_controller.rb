class ApplicationController < ActionController::Base
  # helper :all # include all helpers, all the time
  defined?($upr) or before_filter do
    # grab the backend in case we forget to set it (or if we're using DRb)
    defined?($upr) or ObjectSpace.each_object(Upr::InputWrapper) do |x|
      $upr ||= x.backend
    end
  end

end
