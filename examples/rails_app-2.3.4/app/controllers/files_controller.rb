require 'digest/sha1'

class FilesController < ApplicationController
  # used by jQuery streaming upload progress
  def new
    _sha1_flash_self
  end

  # based on ry dahl's streaming AJAX pull:
  # http://rubyforge.org/pipermail/mongrel-users/2007-July/003747.html
  def pull
    _sha1_flash_self
  end

  def index
  end

  # used by mup-compatible upload progress
  def status
    tmp = $upr.read(params[:upload_id]).inspect
    render :text => "#{Rack::Utils.escape_html(tmp)}\n"
  end

  # used by mup-compatible upload progress
  def progress
    render :update do |page|
      status = $upr.read(params[:upload_id]) and
         page.upload_progress.update(status.length, status.seen)
    end
  end

  # used by mup-compatible upload progress
  def upload
    size, hexdigest = _read_sha1_size
    render :text => "sha1: #{hexdigest}<br />" \
                    "size: #{size}<br />" \
                    '<script type="text/javascript">' \
                    'window.parent.UploadProgress.finish();</script>'
  end

private

  def _read_sha1_size
    file = params[:data] or return [ -1, :unknown ]
    File.unlink(file.path) if file.respond_to?(:path)
    digest = Digest::SHA1.new
    if buf = file.read(16384)
      begin
        digest.update(buf)
      end while file.read(16384, buf)
    end
    [ file.size, digest.hexdigest ]
  end

  def _sha1_flash_self
    if request.post?
      size, hexdigest = _read_sha1_size
      msg = "Successfully upload file (size: #{size}, sha1: #{hexdigest})"
      flash[:notice] = msg
      redirect_to :action => params[:action]
    end
  end

end
