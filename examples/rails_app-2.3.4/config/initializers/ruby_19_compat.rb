# Rails 2.3.4 screwed up cookie sessions under 1.9
# ref: https://rails.lighthouseapp.com/projects/8994/tickets/3144

module ActiveSupport

  class MessageVerifier

    private

      undef_method :secure_compare
      warn "overriding secure_compare to be Ruby 1.9-friendly"

      # constant-time comparison algorithm to prevent timing attacks
      def secure_compare(a, b)
        if a.bytesize == b.bytesize
          result = 0
          j = b.each_byte
          a.each_byte { |i| result |= i ^ j.next }
          result == 0
        else
          false
        end
      end

  end
end if Rails::VERSION::STRING == "2.3.4" && "."[0] == "."
