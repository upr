# Be sure to restart your server when you modify this file

# Specifies gem version of Rails to use when vendor/rails is not present
RAILS_GEM_VERSION = '2.3.4' unless defined? RAILS_GEM_VERSION

# Bootstrap the Rails environment, frameworks, and default configuration
require File.join(File.dirname(__FILE__), 'boot')

Rails::Initializer.run do |config|
  # Skip frameworks you're not going to use. To use Rails without a database,
  # you must remove the Active Record framework.
  config.frameworks -= [ :active_resource, :action_mailer ]
  config.threadsafe!
  config.action_controller.session = { :key => "_upr", :secret => "a"*30 }
  config.gem "moneta"

  # for the purposes of running this example in the upr source tree, we'll
  # just add our working copy of upr to the load_paths, however using the
  # gem is recommended for production for ease-of-management
  if (example = true)
    config.load_paths += %W( #{RAILS_ROOT}/../../lib )
  else
    config.gem "upr"
  end

  # We MUST insert Upr before any parsers that read the body
  # since we need to wrap all calls to "rack.input"
  config.middleware.insert_before('ActionController::ParamsParser',
       'Upr',
       # The default is to use the Moneta::Memory store here.
       # This won't work right if you have multiple processes
       # serving your application, however.
       # :backend => ($upr = Upr::Monitor.new),
       #
       # this enables the use of the ActiveRecord store, see
       # app/models/upr_status.rb.  We pass this as a String
       # because ActiveRecord has not yet been loaded here.
       # :backend => "$upr = UprStatus",

       # You can specify an alternative Moneta-backed store here:
       # :backend => ($upr=Upr::Monitor.new(Moneta::Memory.new)),
       #
       # For mongrel_upload_progress compatibility, we can specify
       # a :drb option instead and leave :backend unset.
       #
       # Avoid DRb or any network-backed Moneta stores when using
       # Revactor unless they're using Revactor-aware network functions
       # :drb => 'druby://127.0.0.1:2999',
       #
       # Update at most once every second (default is every 3 seconds)
       :frequency => 1)
end
