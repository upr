class AddUprStatus < ActiveRecord::Migration
  def self.up
    create_table :upr_statuses do |t|
      t.column :upid, :string, :null => false, :unique => true
      t.column :time, :integer, :null => false
      t.column :seen, :integer, :null => false
      t.column :length, :integer, :null => true
    end
  end

  def self.down
    drop_table :upr_statuses
  end
end
