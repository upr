namespace :upr do
  desc "Performs garbage conllection on tables used by UprStatus"
  task :gc do
    require 'config/environment'
    p UprStatus.gc
  end
end

