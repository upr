require 'test_helper'

class UprStatusTest < ActiveSupport::TestCase
  # Replace this with your real tests.
  test "start with length" do
    assert_kind_of UprStatus, UprStatus.start('abcde', 5)
  end

  test "start without length" do
    assert_kind_of UprStatus, UprStatus.start('abcde', nil)
  end

  test "incr from start" do
    assert_nothing_raised { UprStatus.incr('two', 2) }
    assert_equal 2, UprStatus.read('two').seen
  end

  test "errored" do
    assert UprStatus.read('errored').error?
  end

  test "errored incr no-op" do
    assert UprStatus.incr('errored', 6)
    assert UprStatus.read('errored').error?
  end

  test "finish" do
    assert_kind_of UprStatus, UprStatus.finish('two')
    assert UprStatus.read('two').done?
  end

  test "done?" do
    assert UprStatus.read('done').done?
  end

  test "error!" do
    assert_kind_of UprStatus, UprStatus.error!('two')
    assert UprStatus.read('two').error?
  end

end
