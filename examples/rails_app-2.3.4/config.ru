require 'config/environment'
use Rack::ContentLength
use Rack::Chunked
use Rack::Deflater
use Rails::Rack::Static
run ActionController::Dispatcher.new
