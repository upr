# we need thread-safety for Rails with Revactor
ENV["RAILS_ENV"] = "production"

listen 8080, :tcp_nodelay => true
# we're lazy and are just using the Moneta::Memory store
worker_processes 1
Rainbows! do
  use :Revactor
  worker_connections 1000
end

# unclobber the Rails override of the logger formatter :<
Configurator::DEFAULTS[:logger].formatter = Logger::Formatter.new
