require 'test/unit'
require 'upr'

class MonitorTest < Test::Unit::TestCase
  def setup
    @monitor = Upr::Monitor.new
  end

  def test_start_with_length
    assert_kind_of Upr::Status, @monitor.start('abcde', 5)
    status = @monitor.read('abcde')
    assert_equal 5, status.length
    assert_equal 0, status.seen
    assert ! status.error?
    assert ! status.done?
  end

  def test_start_without_length
    assert_kind_of Upr::Status, @monitor.start('abcde', nil)
    status = @monitor.read('abcde')
    assert_nil status.length
    assert_equal 0, status.seen
    assert ! status.error?
    assert ! status.done?
  end

  def test_to_incr
    assert_kind_of Upr::Status, @monitor.start('abcde', 5)
    status = @monitor.incr('abcde', 2)
    assert_kind_of Upr::Status, status
    assert_equal 2, status.seen
    assert ! status.error?
    assert ! status.done?
    @monitor.incr('abcde', 3)
    assert_equal 5, status.seen
    assert ! status.error?
    assert status.done?
  end

  def test_finish_with_length
    assert_kind_of Upr::Status, status = @monitor.start('abcde', 5)
    @monitor.finish('abcde')
    assert ! status.error?
    assert status.done?
    assert_equal 5, status.seen
    assert_equal 5, status.length
  end

  def test_finish_without_length
    assert_kind_of Upr::Status, status = @monitor.start('abcde', nil)
    @monitor.finish('abcde')
    assert ! status.error?
    assert status.done?
    assert_equal 0, status.seen
    assert_equal 0, status.length
  end

end
